import { config } from 'dotenv'
import TelegramBot from 'node-telegram-bot-api';

import { FlowsData } from './model/FlowsData';
import { getUserIdFromMessage } from './utils';
import createFlowHandlers from './handlers/flowHandlers';
import { comands, routs, routsArr } from './__data__/constants'
import { payRespect, start } from './handlers/common';

config()

const bot = new TelegramBot(process.env.TOKEN, { polling: true });
const flowsData = new FlowsData();

const workflowHandlers = createFlowHandlers(flowsData, bot)

routsArr.forEach(routKey => {
  const handler = async (msg: TelegramBot.Message) => {
    workflowHandlers.initFlow(getUserIdFromMessage(msg), routs[routKey].flowName);
  }

  bot.onText(new RegExp(routs[routKey].cmd + '$'), handler)
  bot.onText(new RegExp(routs[routKey].locale + '$'), handler)
})

bot.on('message', async (msg: TelegramBot.Message) => {
  if(!/^\/\w+/.test(msg.text) && !Object.keys(comands).map(t => comands[t].text).includes(msg.text)) {
    const userId = getUserIdFromMessage(msg);
    flowsData.addUser(msg);

    workflowHandlers.nextStep(userId, msg);
  }
});

bot.onText(/\/f/i, payRespect(bot));
bot.onText(/\/start/, start(bot, flowsData));
