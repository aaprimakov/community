
import { getUserDataFromMessage, getUserIdFromMessage } from '../utils';

interface UserFlowData {
  name?: any;
  step?: any;
  data?: any;
}

interface UserData {
  userData?: any;
  date?: any;
  flow?: UserFlowData
}

const initFlowData: UserFlowData = {
    name: null,
    step: null,
    data: null
}

class FlowsData {
  data: Map<number, UserData>;
    constructor() {
      this.data = new Map()
    }

    addUser(msg) {
      const userId = getUserIdFromMessage(msg)
  
      if(!this.data.has(userId)) {
        this.data.set(userId, {
          userData: getUserDataFromMessage(msg),
          date: msg.date,
          flow: {
            ...initFlowData
          }
        })
      }
    }
  
    getUser(id) {
      return this.data.get(id)
    }
  
    saveUserflowData(userId, userFlowData) {
      const userData = { ...this.data.get(userId) }
  
      userData.flow = { ...userFlowData }
      this.data.set(userId, userData)
    }
  
    resetUserflowData(userId) {
      const userData = { ...this.data.get(userId) }
  
      userData.flow = { ...initFlowData }
      this.data.set(userId, userData)
    }
}

export {
  FlowsData,
  initFlowData
}
