import QRCode from 'qrcode';
import TelegramBot from 'node-telegram-bot-api';

import { getQuizeImage, getQuizePickImage } from '../generate-img/buffer';
import workflow, { Flow } from '../__data__/workflow';
import { FlowsData, initFlowData } from '../model/FlowsData';
import { OPTIONS } from '../__data__/constants'
import { escapeChars } from '../utils'

export default (flowsData: FlowsData, bot: TelegramBot) => {
    const workflowHandlers = {
        initFlow: (userId, flowName) => {
            flowsData.resetUserflowData(userId);
        
            const flow: Flow = workflow.flows[flowName]
            const currentUserFlow = {
                ...initFlowData,
                name: flowName,
                step: flow.init
            }

            flowsData.saveUserflowData(userId, currentUserFlow)
            
            bot.sendMessage(userId, flow.steps[flow.init].t, OPTIONS.CLEAR_KB);
        },

        nextStep: async (userId, msg) => {
            await bot.sendMessage(userId, '...');
            const user = flowsData.getUser(userId);
            const flowName: string = user.flow.name;
            const step: string = user.flow.step;
            const currentFlow: Flow = workflow.flows[flowName];
        
            const nextStep = currentFlow.steps[step].next;

            let nextStepData = {
                ...user.flow.data,
                [step]: msg.text
            }

            if (msg.photo?.length) {
                const fileId = msg.photo.slice(-1)[0].file_id
                const imageLink = await bot.getFileLink(fileId)
            
                nextStepData = {
                    ...user.flow.data,
                    [step]: imageLink
                }
            }

            if (/^image\/(jpg|png)/.test(msg.document?.mime_type)) {
                const fileId = msg.document.file_id
                const imageLink = await bot.getFileLink(fileId)
            
                nextStepData = {
                    ...user.flow.data,
                    [step]: imageLink
                }
            }
        
            const nextUserFlow = {
                ...initFlowData,
                name: flowName,
                step: nextStep,
                data: nextStepData
            }
            flowsData.saveUserflowData(userId, nextUserFlow)
        
            if(nextStep === 'finish') {
                workflowHandlers.finishFlow(userId);
            } else {
                bot.sendMessage(userId, currentFlow.steps[nextStep].t);
            }
        },
        finishFlow: (userId) => {
            const user = flowsData.getUser(userId);
            const flowName = user.flow.name;
            workflowHandlers[`${flowName}Finish`](userId)
        },
        creteQrCodeFinish: async (userId) => {
            const user = flowsData.getUser(userId);
            const { link } = user.flow.data;
        
            const imageBuffer: Buffer = await new Promise((res, rej) => QRCode.toBuffer(link, {
                type: 'jpeg',
                width: 800,
                margin: 2,
                color: {
                    dark: '#130115',
                    light: '#ffffff'
                }
            },(err, buffer) => res(buffer)))
            await bot.sendPhoto(userId, imageBuffer);
            flowsData.resetUserflowData(userId);
            bot.sendMessage(
                userId,
                workflow.flows.creteQrCode.steps.finish.t,
                OPTIONS.COMMANDS_KB
            );
        },
        createPickQuizPicFinish: async (userId) => {
            const user = flowsData.getUser(userId);
        
            const { theme, image } = user.flow.data;
            const imageBuffer = await getQuizePickImage(image, escapeChars(theme));
            
            await bot.sendPhoto(userId, imageBuffer);
            flowsData.resetUserflowData(userId);
            bot.sendMessage(
                userId,
                workflow.flows.createQuizPic.steps.finish.t,
                OPTIONS.COMMANDS_KB
            );
            
        },
        createQuizPicFinish: async (userId) => {
            const user = flowsData.getUser(userId);
        
            const { theme, question } = user.flow.data;
            const imageBuffer = await getQuizeImage(escapeChars(question), escapeChars(theme));
            await bot.sendPhoto(userId, imageBuffer);
            flowsData.resetUserflowData(userId);
            bot.sendMessage(
                userId,
                workflow.flows.createQuizPic.steps.finish.t,
                OPTIONS.COMMANDS_KB
            );
        }
    }

    return workflowHandlers
}