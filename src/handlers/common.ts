import TelegramBot from 'node-telegram-bot-api';

import { getQuizeImage } from '../generate-img/buffer';
import { FlowsData } from '../model/FlowsData';
import { getUserDataFromMessage, getUserIdFromMessage } from '../utils';
import { OPTIONS } from '../__data__/constants';

export const payRespect = (bot: TelegramBot) => async (msg: TelegramBot.Message) => {
    const chatId = msg.chat.id;
  
    const imageBuffer = await getQuizeImage('Respect', '...');
    bot.sendPhoto(chatId, imageBuffer);
}


export const start = (bot: TelegramBot, flowsData: FlowsData) => async (msg: TelegramBot.Message) => {
    const userId = getUserIdFromMessage(msg);
    const user = getUserDataFromMessage(msg);
    flowsData.addUser(msg);

    bot.sendMessage(userId, `
${user.first_name || user.username}, вас приветствует бот, помогающий создавать красивые вопросы для викторин.
Приятного пользования!!
    `, OPTIONS.COMMANDS_KB
);
    const imageBuffer = await getQuizeImage(
      `Так
будет выглядеть
ваш вопрос!`, 'Тут тема');
    bot.sendPhoto(userId, imageBuffer);
}
