import path from 'path'
import fs from 'fs'
import sharp from 'sharp'
import Handlebars from 'handlebars'
import axios from 'axios'

const getTextRow = (line: string): string => `<tspan x="0" dy="1.2em">${line}</tspan>`
const getLines = (text: string): Array<string> => text.split(/\n/)

const templates = {
    question: Handlebars.compile(
        fs.readFileSync(
            path.resolve(__dirname, '../../images/hbs', 'question.svg'),
            'utf-8'
        ), {
        noEscape: true
    }
    )
}

const changeTextInSvg = (svgPath, text, theme) => {
    const lines = getLines(text)
    const xmlLines = lines.map(getTextRow)

    return templates[svgPath]({
        'target-text-place': xmlLines.join('\n'),
        'target-theme-place': theme
    })
}

export const getQuizeImage = async (text, theme) => {
    const changedSvg = changeTextInSvg('question', text, theme)
    let output

    try {
        fs.writeFileSync('aaa.svg', changedSvg)
        output = await new Promise((res, rej) => sharp(Buffer.from(changedSvg))
            .png()
            .toBuffer()
            .then( data => { res(data) })
            .catch( err => { rej(err) })
        )
    } catch (error) {
        console.error('error -->', error)
    }

    return output
}

export const getQuizePickImage = async (imageLink, theme) => {
    const imageBuffer = (await axios({ url: imageLink, responseType: "arraybuffer" })).data as Buffer;
    const changedSvg = changeTextInSvg('question', '', theme)
    let output
    const width = 1160
    const height = 1244

    const resizedImage = await sharp(imageBuffer)
        .resize({
            width: width/1.25,
            height: 414,
            fit: 'inside'
        }).toBuffer()

    try {
        output = await new Promise((res, rej) =>
            sharp(Buffer.from(changedSvg))
                .composite([{
                    input: resizedImage
                }])
                .png()
                .toBuffer()
                .then( data => { res(data) })
                .catch( err => { rej(err) })
        )
    } catch (error) {
        console.error('error ->', error)
    }

    return output
}
