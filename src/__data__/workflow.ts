interface Data {
  flows: Flows;
}

interface Flows {
  [flowName: string]: Flow;
}

export interface Flow {
  init: string;
  steps: {
    finish: Step;
    [step: string]: Step;
  }
}

interface Step {
  t: string;
  next?: string;
}

const workflowsData: Data = {
    flows: {
        createQuizPic: {
            init: 'theme',
            steps: {
                theme: {
                    t: 'Пожалуйста ввидите наименование темы (прим: HTML)',
                    next: 'question'
                },
                question: {
                    t: 'Пожалуйста укажите вопрос. Учтите что переносы строк будут отображаться так же как вы их поставите.\nпример:\n\nА вот интересный\nвопрос для квиза!',
                    next: 'finish'
                },
                finish: {
                    t: 'Готово'
                }
            }
        },
        createPickQuizPic: {
            init: 'theme',
            steps: {
                theme: {
                    t: 'Пожалуйста ввидите наименование темы (прим: HTML)',
                    next: 'image'
                },
                image: {
                    t: 'Теперь отправте картинку',
                    next: 'finish'
                },
                finish: {
                    t: 'Готово'
                }
            }
        },
        creteQrCode: {
            init: 'link',
            steps: {
                link: {
                    t: 'Отправьте мне ссылку\nя превращу ее в qr code',
                    next: 'finish'
                },
                finish: {
                    t: 'Готово'
                }
            }
        }
    }
}

export default workflowsData
