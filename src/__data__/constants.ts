import TelegramBot from 'node-telegram-bot-api';

const locales = {
    quizpic: 'Картинка-вопрос',
    createQr: 'Создать QR',
    picquiz: 'Опрос с картинкой внутри'
}

const routs = {
    new: {
        cmd: '/new',
        locale: locales.quizpic,
        flowName: 'createQuizPic'
    },
    picquiz: {
        cmd: '/picquiz',
        locale: locales.picquiz,
        flowName: 'createPickQuizPic'
    },
    qr: {
        cmd: '/qr',
        locale: locales.createQr,
        flowName: 'creteQrCode'
    }
}

const routsArr = Object.keys(routs)

const comands: { [key: string]: TelegramBot.InlineKeyboardButton } = {
    [routs.new.cmd]: {
        text: locales.quizpic,
    },
    [routs.qr.cmd]: {
        text: locales.createQr,
    },
    [routs.picquiz.cmd]: {
        text: locales.picquiz
    }
}

interface OptionsI {
    COMMANDS_KB: TelegramBot.SendBasicOptions;
    CLEAR_KB: TelegramBot.SendBasicOptions;
}

const OPTIONS: OptionsI = {
    COMMANDS_KB: {
        reply_markup: {
          keyboard: [
            [comands[routs.new.cmd], comands[routs.picquiz.cmd]], [comands[routs.qr.cmd]]
          ]
        }
    },
    CLEAR_KB: ({ 
        reply_markup: {
            remove_keyboard: true
        }
    }),
}

export {
    routs,
    comands,
    OPTIONS,
    routsArr
}