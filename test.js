const sharp = require('sharp')
const fs = require('fs')
const Handlebars = require('handlebars')
const path = require('path')
const axios = require('axios')

const templates = {
    question: Handlebars.compile(
        fs.readFileSync(
            path.resolve(__dirname, './images/hbs', 'question.svg'),
            'utf-8'
        ), {
        noEscape: true
    }
    )
}
const test = async () => {
    // const inimage = fs.readFileSync('./download/file_12.jpg')
    // const inimage = fs.readFileSync('download/file_12.jpg', { encode: 'utf-8' })
    const imageLink = 'https://icdn.lenta.ru/images/2020/09/21/21/20200921212138973/detail_30efcf43395af59daf77810c8267a28b.png'
    const input = (await axios({ url: imageLink, responseType: "arraybuffer" })).data;
    // fs.writeFileSync('./imm.png', input)
    // const inimage = fs.readFileSync('./imm.png')
    // const base64Image = input.toString('base64')
    // const base64Image = inimage.toString('base64') // .replace(/\//g, '_').replace(/\+/g, '-')

    // console.log(111, inimage.toString().slice(0, 120))
    // console.log(222, base64Image.slice(0, 120))

    const svgImage = templates.question({
        'target-text-place':  ' ',
        'target-theme-place': 'no-theme',
    })

    const width = 1160,
        heught = 1244
    const iimage = await sharp(input)
        .resize(width/1.25).toBuffer()

    sharp(Buffer.from(svgImage))
        // .resize(2 * width, 2 * heught)
        // .overlayWith(Buffer.from(svgImage), { density, top, left })
        .composite([{
            input: iimage
        }])
        // .resize(width, heught)
        .png()
        .toFile('./aaa.png')
    // sharp(Buffer.from(svgImage))
    //     .png()
    //     .toFile('./aaa.png')
}

test()